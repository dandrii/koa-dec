import 'reflect-metadata';

import debug from 'debug';
import type Router from '@koa/router';

import type {Scheduler} from './decorators';
import {RequestMethod} from './types';

const log = debug('koa-dec:injector');

export interface Class<T> {
	new (...args: any[]): T;
}

interface Route {
	path: string;
	method: RequestMethod;
	middleware: Router.Middleware;
}

class I {
	private _routes: Route[] = [];
	public get routes(): Route[] {
		return this._routes;
	}
	private resolved = new Map<Class<unknown>, unknown>();
	private _schedulers: Scheduler[] = [];
	public get schedulers() {
		return this._schedulers;
	}

	addControllers(...controllers: Class<any>[]) {
		controllers.forEach((controller) => Injector.resolve(controller));
	}

	route(router: Router) {
		this.routes.forEach((route) => router.register(route.path, [RequestMethod[route.method]], route.middleware));
	}

	resolve<T>(target: Class<T>): T {
		const tokens = (Reflect.getMetadata('design:paramtypes', target) || []) as ConstructorParameters<Class<T>>;
		const injections = tokens.map((token) => Injector.resolve(token));

		const existing = Injector.resolved.get(target);
		if (existing) {
			return existing as T;
		}

		const instance = new target(...injections);

		const path = Reflect.getMetadata('path', target);
		if (path) {
			const prototype = Object.getPrototypeOf(instance);
			const keys = Object.getOwnPropertyNames(prototype);
			for (const key of keys) {
				const route = this.scanForRoutes(target, instance, prototype, key);
				if (route) {
					this.routes.push(route);
				}
			}
		}
		const isScheduler = Reflect.getMetadata('isScheduler', target);
		if (isScheduler) {
			const instanceWithScheduler = instance as T & Scheduler;
			this.schedulers.push(instanceWithScheduler);
			const autoStart = Reflect.getMetadata('autoStart', target);
			if (autoStart) {
				instanceWithScheduler.startScheduler();
			}
		}

		Injector.resolved.set(target, instance);
		return instance;
	}

	private scanForRoutes<T>(constructor: Class<T>, instance: T, prototype: Record<string, unknown>, methodName: string): Route | null {
		const target = prototype[methodName] as Router.Middleware;
		const root = Reflect.getMetadata('path', constructor);
		const method = Reflect.getMetadata('method', target) as RequestMethod;

		if (root === undefined || method === undefined) {
			return null;
		}

		const path = `/${root}/${Reflect.getMetadata('path', target)}`;

		log(`[Method] ${RequestMethod[method].padEnd(5, ' ')} ${path}`);

		return {
			path,
			method,
			middleware: target.bind(instance),
		};
	}
}
/**
 * @example
 * import {Injector} from 'koa-dec';
 * const app = new Koa();
 * 
 * Injector.addControllers(controllerA);
 * // ...
 * Injector.addControllers(controllerB, controllerC);
 * // ...
 * Injector.route(router);
 * 
 * app.use(router.routes());
 */
export const Injector = new I();
