import {RouterContext} from '@koa/router';

export type Context = RouterContext<any, any>;
export type Next = () => Promise<any>;

export enum RequestMethod {
	GET = 0,
	POST,
	PUT,
	DELETE,
	PATCH,
	ALL,
	OPTIONS,
	HEAD,
}
