export interface Scheduler {
	startScheduler: () => void;
	stopScheduler: () => void;
}

/**
 * @param autoStart is true - scheduler will start on Injector run
 * @default NODE_ENV === 'test' ? false : true
 */
export function WithScheduler(autoStart = process.env.NODE_ENV !== 'test'): ClassDecorator {
	return (target) => {
		Reflect.defineMetadata('isScheduler', true, target);
		Reflect.defineMetadata('autoStart', autoStart, target);
	};
}
