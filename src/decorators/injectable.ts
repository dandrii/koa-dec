export function Injectable(): ClassDecorator {
	return (target) => {
		// for now this is useless, might be used later
		Reflect.defineMetadata('scope', 'root', target);
	};
}
