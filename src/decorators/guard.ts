import {Class, Injector} from '../injector';
import {Context} from '../types';

export interface CanActivate<T> {
	canActivate: (ctx: Context, roles?: T[]) => boolean | PromiseLike<boolean>;
}

export const UseGuard = <T>(guard: Class<CanActivate<T>>, roles?: T[]): MethodDecorator => (target, key, descriptor: PropertyDescriptor) => {
	const next = descriptor.value;
	const instance = Injector.resolve(guard);

	descriptor.value = function (ctx: Context) {
		if (instance.canActivate(ctx, roles)) {
			return next.apply(this, [ctx]);
		} else {
			ctx.throw(403);
		}
	};

	return descriptor;
};
