export function Controller(prefix = '/'): ClassDecorator {
	return (target) => {
		Reflect.defineMetadata('path', prefix, target);
	};
}
