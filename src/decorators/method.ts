import {RequestMethod} from '../types';

interface Meta {
	path?: string;
	method: RequestMethod;
}

export const Method = (metadata: Meta): MethodDecorator => (target, key, descriptor) => {
	const path = metadata.path || (key as string);

	Reflect.defineMetadata('path', path, descriptor.value!);
	Reflect.defineMetadata('method', metadata.method, descriptor.value!);
	return descriptor;
};

const createMappingDecorator = (method: RequestMethod) => (path?: string): MethodDecorator =>
	Method({
		path,
		method,
	});

export const Post = createMappingDecorator(RequestMethod.POST);
export const Get = createMappingDecorator(RequestMethod.GET);
export const Delete = createMappingDecorator(RequestMethod.DELETE);
export const Put = createMappingDecorator(RequestMethod.PUT);
export const Patch = createMappingDecorator(RequestMethod.PATCH);
export const All = createMappingDecorator(RequestMethod.ALL);
