import type {Schema} from 'joi';
import type {Context, Next} from '../types';

export interface Validator {
	body?: Schema;
	query?: Schema;
}

function validateBody(ctx: Context, schema: Schema): void {
	const {body} = ctx.request;
	const {error} = schema.validate(body);

	if (error) {
		ctx.throw(400, 'body validation error', {
			data: {details: error.details},
		});
	}
}

function validateQuery(ctx: Context, schema: Schema): void {
	const {query} = ctx.request;
	const {error} = schema.validate(query);

	if (error) {
		ctx.throw(400, 'query validation error', {
			data: {details: error.details},
		});
	}
}

export const isValid = ({body, query}: Validator) => (ctx: Context, next: Next): Promise<void> => {
	if (body) {
		validateBody(ctx, body);
	}
	if (query) {
		validateQuery(ctx, query);
	}

	return next();
};

export const UseValidator = (schema: Validator): MethodDecorator => (target, key, descriptor: PropertyDescriptor) => {
	const next = descriptor.value;
	const validator = isValid(schema);

	descriptor.value = function (ctx: Context) {
		return validator(ctx, next.bind(this, ctx));
	};

	return descriptor;
};
